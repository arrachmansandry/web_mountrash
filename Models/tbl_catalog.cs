﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebMountrash.Models
{
    public class tbl_catalog
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string LastUpdate { get; set; }

        public class CatalogSubType
        {
            public string IdCatalog { get; set; }
            public string Id { get; set; }
            public string Name { get; set; }
            public string LastUpdate { get; set; }

        }
    }
}
