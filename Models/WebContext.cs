﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebMountrash.Models
{

    public class WebContext : DbContext
    {
        public DbSet<tbl_catalog> tbl_Catalogs { get; set; }

        public WebContext(DbContextOptions<WebContext> options) : base(options){}

    }
}
