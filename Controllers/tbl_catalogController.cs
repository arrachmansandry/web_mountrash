﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebMountrash.Models;

namespace WebMountrash.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class tbl_catalogController : ControllerBase
    {
        private readonly WebContext _context;

        public tbl_catalogController(WebContext context)
        {
            _context = context;
        }

        // GET: api/tbl_catalog/GetCatalog
        [HttpGet]
        public async Task<ActionResult<IEnumerable<tbl_catalog>>> GetCatalog()
        {
            return await _context.tbl_Catalogs.ToListAsync();
        }

        [HttpPost]
        public async Task<ActionResult<IEnumerable<tbl_catalog>>> InsertCatalog(tbl_catalog tbl_)
        {
            _context.tbl_Catalogs.Add(tbl_);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (tbl_catalogExists(tbl_.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetCatalog", new { id = tbl_.Id }, tbl_);
        }

        [HttpPost]
        public async Task<ActionResult<IEnumerable<tbl_catalog>>> UpdateCatalog(tbl_catalog tbl_)
        {
            _context.Entry(tbl_).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!tbl_catalogExists(tbl_.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpPost("{Id}")]
        public async Task<ActionResult<tbl_catalog>> DeleteCatalog(string Id)
        {
            var tbl_catalog = await _context.tbl_Catalogs.FindAsync(Id);
            if (tbl_catalog == null)
            {
                return NotFound();
            }

            _context.tbl_Catalogs.Remove(tbl_catalog);
            await _context.SaveChangesAsync();

            return tbl_catalog;
        }

        private bool tbl_catalogExists(string id)
        {
            return _context.tbl_Catalogs.Any(e => e.Id == id);
        }
    }
}
