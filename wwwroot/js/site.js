﻿var todos = [];

function getItems() {
    fetch("api/tbl_catalog/GetCatalog", {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    })
        .then(response => response.json())
        .then(data => _displayItems(data))
        .catch(error => console.error('Unable to get items.', error));
}

function insertItems() {
    var items = {
        "Id": $("#txtId").val(),
        "Name": $("#txtName").val(), 
        "LastUpdate": $("#txtUpdate").val()
    }

    fetch("api/tbl_catalog/InsertCatalog", {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(items)
    })
        .then(response => response.json())
        .then(() => {
            getItems();
            addNameTextbox.value = '';
        })
        .catch(error => console.error('Unable to add item.', error));
}

function updateItems() {
    var items = {
        "Id": $("#txtId").val(),
        "Name": $("#txtName").val(),
        "LastUpdate": $("#txtUpdate").val()
    }

    fetch("api/tbl_catalog/UpdateCatalog", {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(items)
    })
        .then(response => response.json())
        .then(() => {
            getItems();
        })
        .catch(error => console.error('Unable to add item.', error));
}

function deleteItems(Id) {
    console.log(Id)
    //fetch(`api/tbl_catalog/DeleteCatalog/${pid}`, {
    //    method: 'POST'
    //})
    //    .then(() => getItems())
    //    .catch(error => console.error('Unable to delete item.', error));
}

function displayEditForm(id) {
    const item = todos.find(item => item.id === id);

    document.getElementById('edit-name').value = item.name;
    document.getElementById('edit-id').value = item.id;
    document.getElementById('edit-isComplete').checked = item.isComplete;
    document.getElementById('editForm').style.display = 'block';
}

function updateItem() {
    const itemId = document.getElementById('edit-id').value;
    const item = {
        id: parseInt(itemId, 10),
        isComplete: document.getElementById('edit-isComplete').checked,
        name: document.getElementById('edit-name').value.trim()
    };

    fetch('api/tbl_catalog/UpdateCatalog/${id}', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(item)
    })
        .then(() => getItems())
        .catch(error => console.error('Unable to update item.', error));

    closeInput();

    return false;
}

function closeInput() {
    document.getElementById('editForm').style.display = 'none';
}

function _displayCount(itemCount) {
    const name = (itemCount === 1) ? 'to-do' : 'to-dos';

    document.getElementById('counter').innerText = `${itemCount} ${name}`;
}

function _displayItems(data) {
    var tBody = document.getElementById('todos');
    tBody.innerHTML = '';

    _displayCount(data.length);

    var button = document.createElement('button');

    data.forEach(item => {

        var editButton = button.cloneNode(false);
        editButton.innerText = 'Edit';
        editButton.setAttribute('onclick', `displayEditForm(${String(item.id)})`);

        var deleteButton = button.cloneNode(false);
        deleteButton.innerText = 'Delete';
        deleteButton.setAttribute('onclick', `deleteItems(${String(item.id)})`);

        var tr = tBody.insertRow();

        var td1 = tr.insertCell(0);
        var idNode = document.createTextNode(item.id);
        td1.appendChild(idNode);

        var td2 = tr.insertCell(1);
        var nameNode = document.createTextNode(item.name);
        td2.appendChild(nameNode);

        let td3 = tr.insertCell(2);
        td3.appendChild(editButton);

        let td4 = tr.insertCell(3);
        td4.appendChild(deleteButton);
    })

    todos = data;
}